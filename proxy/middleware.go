package proxy

import (
	"net/http"
)

// Middleware проверка URL на соответсвие
func Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/api/login" || r.URL.Path == "/api/register" || r.URL.Path == "/api/address/search" || r.URL.Path == "/api/address/geocode" {
			next.ServeHTTP(w, r)
			return
		}

		http.NotFound(w, r)
	})
}
