package main

import (
	"fmt"
	"github.com/go-chi/chi/v5"
	"github.com/joho/godotenv"
	httpSwagger "github.com/swaggo/http-swagger"
	"log"
	"net/http"
	"os"
	jwt "studentgit.kata.academy/SLK/jsonwebtoken/JWTAuth"
	_ "studentgit.kata.academy/SLK/jsonwebtoken/docs"
	handl "studentgit.kata.academy/SLK/jsonwebtoken/handlers"
	model "studentgit.kata.academy/SLK/jsonwebtoken/models"
)

// @title Поиск данных адреса
// @version 1.0
// @description Данный сервис предоставляент возможность поиска информации об адресе, по его фактическом адресу или геолокации.
// @contact.name Вячеслав
// @contact.email slavalomov.4@gmail.com
// @host localhost:8080
// @BasePath /
// @accept json
// @produce json
// @securitydefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func main() {
	err := godotenv.Load(".env")
	if err != nil {
		fmt.Println("Error loading .env file")
		return
	}

	port := os.Getenv("SERVER_PORT")

	model.TokenAuth = jwt.New("HS256", []byte(os.Getenv("TOKEN_SECRET")), nil)

	log.Fatal(http.ListenAndServe(":"+port, Router(port)))

}

func Router(port string) http.Handler {

	r := chi.NewRouter()

	r.Get("/swagger/*", httpSwagger.Handler(
		httpSwagger.URL("http://localhost:"+port+"/swagger/doc.json"), //The url pointing to API definition
	))
	r.Post("/api/login", handl.LoginHandler)
	r.Post("/api/register", handl.RegisterHandler)
	// Защищенный маршрут, требующий авторизации
	r.Group(func(r chi.Router) {
		r.Use(jwt.Verifier(model.TokenAuth))
		r.Use(jwt.Authenticator(model.TokenAuth))
		r.Post("/api/address/search", handl.HandlerSearch)
		r.Post("/api/address/geocode", handl.HandlerGeo)
	})

	return r
}
