package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"golang.org/x/crypto/bcrypt"
	"io"
	"net/http"
	"net/http/httptest"
	db "studentgit.kata.academy/SLK/jsonwebtoken/DB"
	handl "studentgit.kata.academy/SLK/jsonwebtoken/handlers"
	model "studentgit.kata.academy/SLK/jsonwebtoken/models"
	prox "studentgit.kata.academy/SLK/jsonwebtoken/proxy"
	"testing"
)

type TestCaseReg struct {
	user         model.User
	expectedCode int
	expected     string
}

func Test_RegisterHandler(t *testing.T) {

	testi := []TestCaseReg{
		{
			user: model.User{
				Username: "SLK",
				Password: "123456",
			},
			expectedCode: 201,
			expected:     "Регистрация успешна.",
		},
		{
			user: model.User{
				Username: "SLK",
			},
			expectedCode: 400,
			expected:     "Поле пароля не может быть пустым.",
		},
		{
			user: model.User{
				Password: "123456",
			},
			expectedCode: 400,
			expected:     "Поле, имени пользователя, не может быть пустым.",
		},
		{
			user:         model.User{},
			expectedCode: 400,
			expected:     "Поле, имени пользователя, не может быть пустым.",
		},
	}

	for _, v := range testi {
		dataUser, err := json.Marshal(v.user)
		if err != nil {
			fmt.Println(err)
			return
		}

		req := httptest.NewRequest(http.MethodPost, "/", bytes.NewBuffer(dataUser))
		w := httptest.NewRecorder()
		handl.RegisterHandler(w, req)

		res := w.Result()
		defer res.Body.Close()

		data, _ := io.ReadAll(res.Body)

		// Проверка статус кода по итогам запроса
		if res.StatusCode != v.expectedCode {
			t.Errorf("Ожидаемый статус код: %v\nПолученный статус код: %v", v.expectedCode, res.StatusCode)
		}

		// Проверка полученного от сервиса соощения
		if string(data) != v.expected {
			t.Errorf("Ожидаемый ответ: %s\nПолученный ответ: %s", v.expected, string(data))
		}

		if v.user.Username != "" && v.user.Password != "" {
			// Проверка наличия в карте ключа по имени пользователя
			if _, ok := db.MemoryAuth[v.user.Username]; !ok {
				t.Errorf("Карта пуста или значение в ней записано не по имени пользователя.\nЗначения карты: %v", db.MemoryAuth)
			}

			// Проверка имени пользователя в карте по значению
			if db.MemoryAuth[v.user.Username].Username != v.user.Username {
				t.Errorf("Значение имени пользователя в карте неверно\n Ожидаемое значение: %s\nПолученное значение: %s", v.user.Username, db.MemoryAuth[v.user.Username].Username)
			}

			// Проверка пароля с помощью bcrypt
			pass := []byte(v.user.Password)

			if ok := bcrypt.CompareHashAndPassword([]byte(db.MemoryAuth[v.user.Username].Password), pass); ok != nil {
				t.Errorf("Значение пароля пользоватя в карте неверно\n %v", ok)
			}

		}
	}

}

type TestCaseLog struct {
	userR        model.User
	userL        model.User
	expectedCode int
	expected     string
}

func Test_LoginHandler(t *testing.T) {
	testi := []TestCaseLog{
		{
			userR: model.User{
				Username: "SLK",
				Password: "123456",
			},
			userL: model.User{
				Username: "SLK",
				Password: "12345",
			},
			expectedCode: 200,
			expected:     "Неверный пароль.\n",
		},
		{
			userR: model.User{
				Username: "SLK",
				Password: "123456",
			},
			userL: model.User{
				Username: "SL",
				Password: "123456",
			},
			expectedCode: 200,
			expected:     "Пользователь не найден.\n",
		},
	}

	for _, v := range testi {
		dataUser, err := json.Marshal(v.userR)
		if err != nil {
			fmt.Println(err)
			return
		}

		req := httptest.NewRequest(http.MethodPost, "/", bytes.NewBuffer(dataUser))
		w := httptest.NewRecorder()
		handl.RegisterHandler(w, req)

		dataUserL, err := json.Marshal(v.userL)
		if err != nil {
			fmt.Println(err)
			return
		}

		req2 := httptest.NewRequest(http.MethodPost, "/", bytes.NewBuffer(dataUserL))
		w2 := httptest.NewRecorder()
		handl.LoginHandler(w2, req2)

		res := w2.Result()
		defer res.Body.Close()

		data, _ := io.ReadAll(res.Body)

		if string(data) != v.expected {
			t.Errorf("Ожидаемый ответ: %s\nПолученный ответ: %s", v.expected, string(data))
		}

	}
}

func Test_HandlerGeo(t *testing.T) {
	geo := model.GeocodeRequest{
		Lat: 55.878,
		Lon: 37.653,
	}

	dataGeo, err := json.Marshal(geo)
	if err != nil {
		fmt.Println(err)
		return
	}

	req := httptest.NewRequest(http.MethodPost, "/", bytes.NewBuffer(dataGeo))
	req.Header.Set("contentType", "application/json")
	w := httptest.NewRecorder()
	handl.HandlerGeo(w, req)

	res := w.Result()
	defer res.Body.Close()

	data, _ := io.ReadAll(res.Body)

	expectedStatusCode := "200 OK"
	if res.Status != expectedStatusCode {
		t.Errorf("Ожидаемый статус код: %s\nПолученный статус код: %s", expectedStatusCode, res.Status)
	}

	expectedData := "[{\"city\":\"Москва\",\"street\":\"Сухонская\",\"house\":\"11\",\"geo_lat\":\"55.878315\",\"geo_lon\":\"37.65372\"},{\"city\":\"Москва\",\"street\":\"Сухонская\",\"house\":\"11А\",\"geo_lat\":\"55.878212\",\"geo_lon\":\"37.652016\"},{\"city\":\"Москва\",\"street\":\"Сухонская\",\"house\":\"13\",\"geo_lat\":\"55.878666\",\"geo_lon\":\"37.6524\"},{\"city\":\"Москва\",\"street\":\"Сухонская\",\"house\":\"9\",\"geo_lat\":\"55.877167\",\"geo_lon\":\"37.652481\"},{\"city\":\"Москва\",\"street\":\"\",\"house\":\"\",\"geo_lat\":\"55.75396\",\"geo_lon\":\"37.620393\"}]"
	if string(data) != expectedData {
		t.Errorf("Ожидаемый ответ: %s\nПолученный ответ: %s", expectedData, string(data))
	}
}

func Test_HandlerSearch(t *testing.T) {
	search := model.SearchRequest{
		Query: "москва сухонская 11",
	}

	dataSearch, err := json.Marshal(search)
	if err != nil {
		fmt.Println(err)
		return
	}

	req := httptest.NewRequest(http.MethodPost, "/", bytes.NewBuffer(dataSearch))
	req.Header.Set("contentType", "application/json")
	w := httptest.NewRecorder()
	handl.HandlerSearch(w, req)

	res := w.Result()
	defer res.Body.Close()

	data, _ := io.ReadAll(res.Body)

	expectedStatusCode := "200 OK"
	if res.Status != expectedStatusCode {
		t.Errorf("Ожидаемый статус код: %s\nПолученный статус код: %s", expectedStatusCode, res.Status)
	}

	expectedData := "[{\"city\":\"\",\"street\":\"Сухонская\",\"house\":\"11\",\"geo_lat\":\"55.8782557\",\"geo_lon\":\"37.65372\"}]"
	if string(data) != expectedData {
		t.Errorf("Ожидаемый ответ: %s\nПолученный ответ: %s", expectedData, string(data))
	}
}

type testCaseMidlleware struct {
	url             string
	expectedCode    int
	expectedRequest string
}

func Test_middlewares(t *testing.T) {

	testsi := []testCaseMidlleware{
		{
			url:             "/api/address/search",
			expectedCode:    200,
			expectedRequest: "TEST",
		},
		{
			url:             "/api/address/geocode",
			expectedCode:    200,
			expectedRequest: "TEST",
		},
		{
			url:             "/",
			expectedCode:    404,
			expectedRequest: "404 page not found\n",
		},
	}

	for _, val := range testsi {
		req := httptest.NewRequest(http.MethodGet, val.url, nil)

		rr := httptest.NewRecorder()

		hadler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			_, err := w.Write([]byte("TEST"))
			if err != nil {
				t.Fatal(err)
			}
		})

		prox.Middleware(hadler).ServeHTTP(rr, req)

		if val.expectedCode != rr.Code {
			t.Errorf("Ожидал: %v \n Получил: %v", val.expectedCode, rr.Code)
		}

		if val.expectedRequest != rr.Body.String() {
			t.Errorf("Ожидал: %s \n Получил: %s", val.expectedRequest, rr.Body.String())
		}
	}

}
