package db

import model "studentgit.kata.academy/SLK/jsonwebtoken/models"

// MemoryAuth база данных для хранения пользователей в памяти
var MemoryAuth = map[string]model.User{}
