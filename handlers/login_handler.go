package handlers

import (
	"encoding/json"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	dataB "studentgit.kata.academy/SLK/jsonwebtoken/DB"
	model "studentgit.kata.academy/SLK/jsonwebtoken/models"
)

// LoginHandler аторизация пользователя.
// @Summary аторизация пользователя.
// @Tags Авторизация
// @Description Авторизует пользователя и предоставляет ему токен для доступа к ресурсам сервиса.
// @Param input body models.User true "координаты".
// @Success      200  {string}  string "token: ..."
// @Success      200  {string}  string "Пользователь не найден."
// @Success      200  {string}  string "Неверный пароль."
// @Failure      400  {string}  string "400 : неверный формат запроса."
// @Router       /api/login [post]
func LoginHandler(w http.ResponseWriter, r *http.Request) {
	var user model.User

	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, "400:  неверный формат запроса.", http.StatusBadRequest)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	storedUser, ok := dataB.MemoryAuth[user.Username]
	if !ok {
		http.Error(w, "Пользователь не найден.", http.StatusOK)
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(storedUser.Password), []byte(user.Password))
	if err != nil {
		http.Error(w, "Неверный пароль.", http.StatusOK)
		return
	}

	_, token, _ := model.TokenAuth.Encode(map[string]interface{}{"username": user.Username, "password": user.Password})

	w.Write([]byte("token: " + token))
	//json.NewEncoder(w).Encode(map[string]interface{}{"token": token})
}
