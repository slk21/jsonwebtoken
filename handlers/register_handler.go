package handlers

import (
	"encoding/json"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	dataB "studentgit.kata.academy/SLK/jsonwebtoken/DB"
	model "studentgit.kata.academy/SLK/jsonwebtoken/models"
)

// RegisterHandler обрабытывает запросы на регистрацию пользователей
// @Summary регистрация пользователя.
// @Tags Авторизация
// @Description Регистрирует пользователя по логину и паролю..
// @Param input body models.User  true "логин и пароль".
// @Success      201  {string}  string "Регистрация успешна."
// @Failure      400  {string}  string "400:  неверный формат запроса."
// @Failure      400  {string}  string "Поле, имени пользователя, не может быть пустым."
// @Failure      400  {string}  string "Поле пароля не может быть пустым."
// @Router       /api/register [post]
func RegisterHandler(w http.ResponseWriter, r *http.Request) {
	var user model.User

	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("400:  неверный формат запроса."))
		return
	}

	if user.Username == "" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Поле, имени пользователя, не может быть пустым."))
		return
	} else if user.Password == "" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Поле пароля не может быть пустым."))
		return
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	user.Password = string(hashedPassword)
	dataB.MemoryAuth[user.Username] = user
	w.WriteHeader(http.StatusCreated)
	w.Write([]byte("Регистрация успешна."))

}
