package handlers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	model "studentgit.kata.academy/SLK/jsonwebtoken/models"
)

// HandlerGeo отправляет запрос на dadata.ru и возвращает полученый ответ клиенту.
// @Summary Получение данных адреса.
// @Security ApiKeyAuth
// @Tags Даннные по адресам
// @Description Поиск информации об адресе по его геолокационным данным.
// @Param input body models.GeocodeRequest  true "координаты".
// @Success      200  {object}  models.GeocodeResponse "информация об адресе"
// @Failure      400  {string}  string "400 :Неверный формат запроса"
// @Failure      500  {string}  string "500: Сервис dadata.ru не доступен"
// @Router       /api/address/geocode [post]
func HandlerGeo(w http.ResponseWriter, r *http.Request) {
	bodyJS, errIRA := io.ReadAll(r.Body)
	if errIRA != nil {
		fmt.Println(errIRA)
		return
	}

	client := &http.Client{}

	req, errNR := http.NewRequest("POST", "http://suggestions.dadata.ru/suggestions/api/4_1/rs/geolocate/address", bytes.NewBuffer(bodyJS))
	if errNR != nil {
		fmt.Println(errNR)
		return
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", "Token ac997d872492c68962359e7f3d3ddc285cf0901c")
	req.Header.Set("X-Secret", "8b79360c8a2f1c712359817167374661a0acd9cf")

	resp, errCD := client.Do(req)
	if errCD != nil {
		fmt.Println(errCD)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("500: Сервис dadata.ru не доступен"))
		return
	}
	defer resp.Body.Close()

	var geoCode model.GeoCode

	errJND := json.NewDecoder(resp.Body).Decode(&geoCode)
	if errJND != nil {
		fmt.Println(errJND)
		return
	}

	var res []model.Address
	for _, r := range geoCode.Suggestions {
		var address model.Address
		address.City = string(r.Data.City)
		address.Street = string(r.Data.Street)
		address.House = r.Data.House
		address.Lat = r.Data.GeoLat
		address.Lon = r.Data.GeoLon

		res = append(res, address)
	}

	resJSON, errJM := json.Marshal(res)
	if errJM != nil {
		fmt.Println(errJM)
		return
	}

	w.WriteHeader(http.StatusOK)
	_, err := w.Write(resJSON)
	if err != nil {
		fmt.Println(err)
		return
	}
}
