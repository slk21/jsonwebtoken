package models

import (
	jwt "studentgit.kata.academy/SLK/jsonwebtoken/JWTAuth"
)

// User представление пользователя в памяти
type User struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

var TokenAuth *jwt.JWTAuth
